# Notes on... stuff

In this repository I collect notes and snippets other people might find interesting.
I'm not an expert on most things I write about here, so take everything with a truckload of salt.

Quicklinks:
- [Notes on the history of blackbody radiation theory](https://kschwenk.gitlab.io/skinner/blackbody_history.html)

An index is available at the [GitLab Pages page](https://kschwenk.gitlab.io/skinner) or as a [local html export](./index.html), or as an [Org file](./index.org).


## License

[![CC0](http://i.creativecommons.org/p/zero/1.0/88x31.png "CC0")](http://creativecommons.org/publicdomain/zero/1.0/)

[CC0 1.0 Universal public domain dedication](CC0 1.0 Universal public domain dedication):
To the extent possible under law, the authors have waived all copyright and related or neighboring rights to this project.
This work is published from: Germany.
